package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/gofiber/fiber/v2"
	_ "github.com/lib/pq"

	"app/service"
)

func main() {
	log.Print("started")

	app := service.NewApp(&service.Config{
		RedisServer:      os.Getenv("REDIS_ADDR"),
		RedisPassword:    os.Getenv("REDIS_PASS"),
		BeanstalkdServer: os.Getenv("BEANSTALKD_ADDR"),
	})

	server := fiber.New()

	server.Get("/push", app.Push)
	server.Get("/stats", app.Stats)

	go log.Panic(server.Listen(":9000"))
	waitKill()

	log.Print("stopped")
}

func waitKill() {
	waitKill := make(chan os.Signal, 1)

	signal.Notify(waitKill,
		os.Interrupt,
		syscall.SIGTERM,
		syscall.SIGINT,
		syscall.SIGSTOP,
		syscall.SIGKILL,
		syscall.SIGHUP,
		syscall.SIGQUIT,
	)

	<-waitKill
}
