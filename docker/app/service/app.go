package service

import (
	"fmt"
	"os"

	"github.com/gofiber/fiber/v2"
	_ "github.com/lib/pq"
)

type PupSub interface {
	Subscribe(topic string)
	Publish(topic, msg string)
	Connect()
	Close()
	Stats() (string, error)
}

type app struct {
	pubSub PupSub
}

const (
	topic             = "tester"
	defaultMsgPayload = "test message"
)

func NewApp(config *Config) *app {
	var pubSub PupSub

	if config.BeanstalkdServer != "" {
		pubSub = &Beanstalk{
			ServerAddress: config.BeanstalkdServer,
		}
		pubSub.Connect()
		go pubSub.Subscribe(topic)

	} else if config.RedisServer != "" {
		pubSub = &RedisClient{
			ServerAddr: config.RedisServer,
			Password:   config.RedisPassword,
		}
		pubSub.Connect()
		go pubSub.Subscribe(topic)
	} else {
		fmt.Println("Has neither Beanstalk nor Redis config")
		os.Exit(1)
	}

	return &app{
		pubSub: pubSub,
	}
}

func (a *app) Close() {
	a.pubSub.Close()
}

func (a *app) Push(c *fiber.Ctx) error {
	a.pubSub.Publish(topic, defaultMsgPayload)
	return nil
}

func (a *app) Stats(c *fiber.Ctx) error {
	s, err := a.pubSub.Stats()
	if err != nil {
		return err
	}
	c.WriteString(s)
	return nil
}
