package service

type Config struct {
	RedisServer      string
	RedisPassword    string
	BeanstalkdServer string
}
