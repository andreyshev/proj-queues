package service

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/redis/go-redis/v9"
)

type RedisClient struct {
	ServerAddr string
	Password   string

	client *redis.Client
}

func (r *RedisClient) Connect() {
	rdb := redis.NewClient(&redis.Options{
		//Addr:     "redis:6379",
		//Password: "password",
		Addr:     r.ServerAddr,
		Password: r.Password,
		DB:       0,
	})

	if err := rdb.Ping(context.TODO()).Err(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	r.client = rdb
}

func (r *RedisClient) Close() {
	r.client.Close()
}

func (r *RedisClient) Subscribe(channel string) {
	ctx := context.Background()
	subscriber := r.client.Subscribe(ctx, channel)
	for {
		_, err := subscriber.ReceiveMessage(ctx)
		if err != nil {
			log.Println(err)
		}
	}
}

func (r *RedisClient) Stats() (string, error) {
	return "", nil
}

func (r *RedisClient) Publish(tube, msg string) {
	cmd := r.client.Publish(context.TODO(), tube, msg)
	if err := cmd.Err(); err != nil {
		log.Println(err)
	}
}
