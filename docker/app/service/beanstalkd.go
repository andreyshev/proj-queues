package service

import (
	"fmt"
	"log"
	"os"
	"time"

	gobeanstalk "github.com/beanstalkd/go-beanstalk"
)

type Beanstalk struct {
	ServerAddress          string
	serverConnection       *gobeanstalk.Conn
	serverConnectionSubscr *gobeanstalk.Conn
}

func (b *Beanstalk) Connect() {
	beanstalkConnection, err := gobeanstalk.Dial("tcp", b.ServerAddress)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	serverConnectionSubscr, err := gobeanstalk.Dial("tcp", b.ServerAddress)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println("Beanstalk connected!")
	b.serverConnection = beanstalkConnection
	b.serverConnectionSubscr = serverConnectionSubscr
}

func (b *Beanstalk) Close() {
	if b.serverConnection != nil {
		b.serverConnection.Close()
	}
}

func (b *Beanstalk) Stats() (string, error) {
	return b.Stats()
}

func (b *Beanstalk) Subscribe(tube string) {
	for {
		id, _, err := b.serverConnectionSubscr.Reserve(10 * time.Second)
		if err != nil {
			fmt.Println(err)
			continue
		}
		//fmt.Println("processed job id: ", string(body))
		if err := b.serverConnectionSubscr.Delete(id); err != nil {
			fmt.Println(err)
		}
	}
}

func (b *Beanstalk) Publish(tube, msg string) {
	_, err := b.serverConnection.Put([]byte(msg), 0, 0, 10*time.Second)
	if err != nil {
		log.Println(err)
	}
}
