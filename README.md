# Proj Queues


```siege -c 30 -t 2M http://localhost:9000/push```

|                    | Requests | Availability | Requests rate |
|--------------------|----------|--------------|---------------|
| Beanstalkd         | 65551    | 99.91        | 542.33        |
| Beanstalkd persist | 65541    | 99.90        | 543.46        |
| Redis RDB          | 65572    | 99.90        | 542.50        |
| Redis AOF          | 65495    | 99.93        | 541.46        |

```siege -c 50 -t 2M http://localhost:9000/push```

|                    | Requests | Availability | Requests rate |
|--------------------|----------|--------------|---------------|
| Beanstalkd         | 65633    | 99.86        | 543.72        |
| Beanstalkd persist | 65545    | 99.85        | 546.07        |
| Redis RDB          | 52931    | 99.79        | 440.87        |
| Redis AOF          | 65619    | 99.84        | 544.65        |

```siege -c 150 -t 2M http://localhost:9000/push```

|                    | Requests | Availability | Requests rate |
|--------------------|----------|--------------|---------------|
| Beanstalkd         | 54919    | 99.80        | 454.89        |
| Beanstalkd persist | 65801    | 99.38        | 546.70        |
| Redis RDB          | 65922    | 99.45        | 548.25        |
| Redis AOF          | 65662    | 99.22        | 546.05        |